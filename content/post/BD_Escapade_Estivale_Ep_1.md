---
title: "BD : Escapade Estivale - Episode 1"
date: 2018-08-10T17:36:37+02:00
lastmod: 2018-08-10T17:36:37+02:00
cover: "/images/BDs/Escapade_estivale/Episode_1/cover.jpg"
draft: false
categories: "bandes dessinées"
tags: "Escapade Estivale"
description: "Premier épisode d'une aventure inédite!"
---
Bienvenue sur le premier article d'une petite série évoquant mon voyage très particuler : j'ai décidé de partir tout nu dans un champs de figuier pas loin du village de Barbentane dans le but d'aller faire rebondir un ballon au sommet du mont Ventoux ! Nous voici donc au début de l'aventure.
Bonne lecture!

**Objectif de cet épisode** : Trouver des vêtements

![planche 1](/images/BDs/Escapade_estivale/Episode_1/1-1-1.jpg)
![planche 2](/images/BDs/Escapade_estivale/Episode_1/1-1-2.jpg)

![planche 3](/images/BDs/Escapade_estivale/Episode_1/1-2-1.jpg)
![planche 4](/images/BDs/Escapade_estivale/Episode_1/1-2-2.jpg)

![planche 5](/images/BDs/Escapade_estivale/Episode_1/1-3-1.jpg)
![planche 6](/images/BDs/Escapade_estivale/Episode_1/1-3-2.jpg)

![planche 7](/images/BDs/Escapade_estivale/Episode_1/1-4-1.jpg)
![planche 8](/images/BDs/Escapade_estivale/Episode_1/1-4-2.jpg)

![planche 9](/images/BDs/Escapade_estivale/Episode_1/2-1-1.jpg)
![planche 10](/images/BDs/Escapade_estivale/Episode_1/2-1-2.jpg)

![planche 11](/images/BDs/Escapade_estivale/Episode_1/2-2-1.jpg)
![planche 12](/images/BDs/Escapade_estivale/Episode_1/2-2-2.jpg)

![planche 13](/images/BDs/Escapade_estivale/Episode_1/2-3-1.jpg)
![planche 14](/images/BDs/Escapade_estivale/Episode_1/2-3-2.jpg)

![planche 15](/images/BDs/Escapade_estivale/Episode_1/2-4-1.jpg)
![planche 16](/images/BDs/Escapade_estivale/Episode_1/2-4-2.jpg)

![Fin](/images/BDs/Escapade_estivale/Episode_1/Fin.jpg)

Merci à André, ancien gendarme, qui fait 80 Km de vélo par jour, a construit un studio tout seul, n'a peur de rien, s'est laissé faire quand, plus jeune, une fille est venue l'embrasser en le confondant avec son petit ami.

To be continued...
