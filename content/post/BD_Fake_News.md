---
title: "BD : Fake News"
date: 2018-08-10T17:36:37+02:00
lastmod: 2018-08-10T17:36:37+02:00
cover: "/images/BDs/Fake_News/cover.jpg"
draft: false
categories: "bandes dessinées"
description: "Qui donc est mieux placé que ce cher Père Castor pour raconter des histoires?"
---
Qui d'autre ? Qui d'autre que le grand père castor peut raconter aussi bien des histoires? Mais comment fait-il pour avoir autant d'inspiration?


![planche 1](/images/BDs/Fake_News/1-1.jpg)
![planche 1bis](/images/BDs/Fake_News/1-2.jpg)
![planche 2](/images/BDs/Fake_News/2-1.jpg)
![planche 2bis](/images/BDs/Fake_News/2-2.jpg)
![planche 3](/images/BDs/Fake_News/3-1.jpg)
![planche 3bis](/images/BDs/Fake_News/3-2.jpg)
![planche 4](/images/BDs/Fake_News/4-1.jpg)
![planche 4bis](/images/BDs/Fake_News/4-2.jpg)
![planche 5bis](/images/BDs/Fake_News/5-1.jpg)
![planche 5bis](/images/BDs/Fake_News/5-2.jpg)
![Fin](/images/BDs/Fake_News/Fin.jpg)
