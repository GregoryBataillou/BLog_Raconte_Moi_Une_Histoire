---
title: Turtle Paille
date: 2018-09-02T18:17:39+02:00
lastmod: 2018-09-02T18:17:39+02:00
cover: "/images/BDs/Turtle_paille/cover.jpg"
draft: false
categories: "bandes dessinées"
tags: "environnement"
description: "L'origine d'une tragique aventure qui aura bouleversé l'opinion publique"
---

![planche 1](/images/BDs/Turtle_paille/1.jpg)
![planche 2](/images/BDs/Turtle_paille/2.jpg)
![planche 3](/images/BDs/Turtle_paille/3.jpg)
![planche 4](/images/BDs/Turtle_paille/4.jpg)

{{< youtube 4wH878t78bw >}}
