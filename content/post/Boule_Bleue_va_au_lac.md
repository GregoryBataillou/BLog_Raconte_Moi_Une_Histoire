---
title: "Boule Bleue va au lac"
date: 2018-08-12T13:37:45+02:00
lastmod: 2018-08-12T13:37:45+02:00
cover: "/images/StopMotion/Boule_Bleue/coverLac.jpg"
draft: false
categories: "Stop Motion"
tags: "Boule Bleue"
description: "Boule Bleue profite de ses vacances au lac."
---

Boule Bleue profite des vacances, alors on en profite aussi.

{{< youtube Cdmu6oor-oQ >}}

Pensez à vous abonner à la [chaîne youtube](https://www.youtube.com/channel/UCYz8h8BpTC7wFH77MlbDLSA") !
