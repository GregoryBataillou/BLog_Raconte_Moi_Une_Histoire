---
title: "Les Tableaux en strip #1 "
date: 2018-11-07T23:42:18+01:00
lastmod: 2018-11-07T23:42:18+01:00
cover: "/images/BDs/Tableaux/Joconde/cover.jpg"
draft: false
categories: "bandes dessinées"
tags: "tableaux"
description: Fuis moi je te suis...
---

![Vignette 1](/images/BDs/Tableaux/Joconde/Joconde_strip_1.jpg)
![Vignette 2](/images/BDs/Tableaux/Joconde/Joconde_strip_2.jpg)
![Vignette 3](/images/BDs/Tableaux/Joconde/Joconde_strip_3.png)

**Informations sur le tableau**

* *Nom* : La Joconde, ou Portrait de Mona Lisa
* *Peintre* : Léonard de Vinci
* *Année* : 1500-1520
* *Lieu d'exposition* : Musée du Louvre, Paris

**Le saviez-vous?**

[Le tableau de la joconde a été volé en 1911](http://www.paris1900.fr/un-peu-dhistoire/vol-de-la-joconde)!
L'homme qui l'a volé, Vincenzo Perugia, un vitrier italien, l'a gardé chez lui pendant plus de 2 ans dans le double fond d'une
valise.
