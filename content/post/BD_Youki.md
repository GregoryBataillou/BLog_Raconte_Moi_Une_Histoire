---
title: Interprétation
date: 2018-08-20T15:34:22+02:00
lastmod: 2018-08-20T15:34:22+02:00
cover: "/images/BDs/Interpretation/cover.jpg"
draft: false
categories: "bandes dessinées"
description: "Youki comprend tout de travers. Peut-on lui en vouloir? Vous avez 2h."
---

![planche 1](/images/BDs/Interpretation/BD_Youki_1.jpg)
![planche 2](/images/BDs/Interpretation/BD_Youki_2.jpg)
