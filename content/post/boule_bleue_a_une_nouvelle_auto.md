---
title: Boule Bleue a une nouvelle auto
date: 2018-08-12T18:35:40+02:00
lastmod: 2018-08-12T18:35:40+02:00
cover: "/images/StopMotion/Boule_Bleue/coverNouvelleAuto.jpg"
draft: false
categories: "Stop Motion"
tags: "Boule Bleue"
description: "Boule Bleue a investi dans une belle auto!"
---

Quand Boule Bleue sort son bolide, les choses dégénèrent..

{{< youtube hfZ0manW6jg >}}

Pensez à vous abonner à la [chaîne youtube](https://www.youtube.com/channel/UCYz8h8BpTC7wFH77MlbDLSA") !
