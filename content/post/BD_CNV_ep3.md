---
title: La Communication Non-Violente - épisode 3
date: 2018-11-16T00:12:02+01:00
lastmod: 2018-11-16T00:12:02+01:00
cover: "/images/BDs/CNV/Ep_3/cover.jpg"
draft: false
categories: "bandes dessinées"
tags: "CNV"
description: ou Comment exprimer plus ses sentiments
---

Ce troisième épisode de la Communication non-Violente est consacré à la notion de sentiment. On y explique pourquoi il est fondamental de les
exprimer de la façon la plus sincère possible, car ne pas les exprimer
implique souvent des frustrations à l'origine de nos jugements.


Bonne lecture!

![Vignette de titre](/images/BDs/CNV/Ep_3/0 - titre.jpg)
![Vignette 1](/images/BDs/CNV/Ep_3/1.jpg)
![Vignette 2](/images/BDs/CNV/Ep_3/2.jpg)
![Vignette 3](/images/BDs/CNV/Ep_3/3.jpg)
![Vignette 4](/images/BDs/CNV/Ep_3/4.jpg)
![Vignette 5](/images/BDs/CNV/Ep_3/5.jpg)
![Vignette 6](/images/BDs/CNV/Ep_3/6.jpg)
![Vignette 7](/images/BDs/CNV/Ep_3/7.jpg)
![Vignette 8](/images/BDs/CNV/Ep_3/8.jpg)
![Vignette 9](/images/BDs/CNV/Ep_3/9.jpg)
![Vignette 10](/images/BDs/CNV/Ep_3/10.jpg)
![Vignette 10](/images/BDs/CNV/Ep_3/10-2.jpg)
![Vignette 10](/images/BDs/CNV/Ep_3/10-3.jpg)
![Vignette 10](/images/BDs/CNV/Ep_3/10-4.jpg)
![Vignette 10](/images/BDs/CNV/Ep_3/10-5.jpg)
![Vignette 10](/images/BDs/CNV/Ep_3/10-6.jpg)
![Vignette 11](/images/BDs/CNV/Ep_3/11.jpg)
![Vignette 12](/images/BDs/CNV/Ep_3/12.jpg)
![Vignette 13](/images/BDs/CNV/Ep_3/13.jpg)
![Vignette 14](/images/BDs/CNV/Ep_3/14.jpg)
![Vignette 11](/images/BDs/CNV/Ep_3/15.jpg)
![Vignette 12](/images/BDs/CNV/Ep_3/16.jpg)
![Vignette 12](/images/BDs/CNV/Ep_3/16-2.jpg)
![Vignette 13](/images/BDs/CNV/Ep_3/17.jpg)
![Vignette 13](/images/BDs/CNV/Ep_3/17-2.jpg)
![Vignette 14](/images/BDs/CNV/Ep_3/18.jpg)

Voici une série de 10 phrases. Certaines sont des expressions des sentiments réels, tandis que les autres sont des jugements ou des interprétations mentales.

Cochez les phrases qui vous semblent être des des sentiments :

<div id="quizz">
</div>

<input type="submit" value="voir les résultats" id="soumission" style="
    background-color: green;
    border: none;
    color: white;
    padding: 2% 3%;
    margin: 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;">

![Vignette 14](/images/BDs/CNV/Ep_3/30.jpg)

<script>

function displayTrueOrFalse(tableauReponses, firstTime=false) {

  for (i = 1; i < Object.keys(tableauQuestionsReponses).length + 1; i++) {
    var checkBox = document.getElementById("checkbox"+i);
    var text = document.getElementById("reponse"+i);

    // If the checkbox is checked, display the output text
    if (checkBox.checked == true){
      text.style.display = "block";
      text.style.color = tableauReponses[i][2];
    } else if ((tableauReponses[i][2] === "green" ) && (!firstTime)) {
      text.style.display = "block";
      text.style.color = "red";
    } else {
      text.style.display = "none";
    }
  }
}


var tableauQuestionsReponses = {
  1 : [
    "J'ai le sentiment que tu ne m'aimes pas",
    "Pas d'accord. Tu ne m'aimes pas est une interprétation de l'action d'un autre. Comme on l'a vu dans la BD, la phrase \"J'ai le sentiment que...\" exprime rarement un sentiment. On aurait pu dire à la place \"Je me sens triste\" par exemple",
    "red"
  ],
  2 : [
    "Je suis triste que tu partes",
    "C'est un sentiment",
    "green"
  ],
  3: [
    "J'ai peur quand tu dis cela",
    "C'est un sentiment",
    "green"
  ],
  4 : [
    "Quand tu ne dis pas bonjour, je me sens délaissée",
    "Pas d'accord! la formulation est correcte, mais l'adjectif 'délaissée' renvoie à l'interprétation de l'action d'autres personnes",
    "red"
  ],
  5 : [
    "Je suis content que tu puisses venir",
    "On y voit un sentiment",
    "green"
  ],
  6 : [
    "Tu es exaspérant",
    "Pas d'accord. Si vous avez suivi l'épisode 2, cette phrase relève d'un jugement moral et non d'une simple observation",
    "red"
  ],
  7: [
    "Je sens que j'ai envie de te taper dessus",
    "pas d'accord, bien que je comprenne le raisonnement. Une envie n'est pas un sentiment. On aurait pu dire 'Je suis furieux contre toi'",
    "red"
  ],
  8 : [
    "Je me sens bien après ce que tu as fait pour moi.",
    "C'est bien un sentiment. Le mot 'bien' est quand même relativement vague.",
    "green"
  ],
  9 : [
    "Je me sens incompris",
    "pas d'accord. L'adjectif 'incompris' représente une interprétation des actions des autres. On aurait pu dire 'je me sens déçu', ou bien 'je suis découragé'",
    "red"
  ],
  10 : [
    "Je me sens incapable",
    "Pas d'accord! Celui qui parle dit ce qu'il pense de lui-même et non ce qu'il ressent. On aurait pu dire 'Je suis démoralisé' par exemple",
    "red"
  ]
}

var submitButton = document.getElementById("soumission");
var quizzCadre = document.getElementById("quizz");


for (i=1; i<Object.keys(tableauQuestionsReponses).length + 1; i ++) {

  var inputI = document.createElement("INPUT");
  inputI.setAttribute("type", "checkbox");
  inputI.setAttribute("id", "checkbox" + i);
  inputI.setAttribute("name", "checkbox" + i);  

  var labelI = document.createElement("LABEL");
  labelI.setAttribute("for", "checkbox" + i);
  var observationI = document.createTextNode("\n" + tableauQuestionsReponses[i][0] + "\n")
  labelI.appendChild(observationI);

  var reponseI = document.createElement("P");
  reponseI.setAttribute("id", "reponse" + i);
  var reponseTextI = document.createTextNode("\n" + tableauQuestionsReponses[i][1] + "\n")
  reponseI.appendChild(reponseTextI);

  var divInputI = document.createElement("DIV");
  divInputI.appendChild(inputI);
  divInputI.appendChild(labelI);
  divInputI.appendChild(reponseI);

  quizzCadre.appendChild(divInputI);

};

displayTrueOrFalse(tableauQuestionsReponses, firstTime=true)

submitButton.addEventListener("click", function () {
  if (this.value === "voir les résultats") {
    displayTrueOrFalse(tableauQuestionsReponses);
  }
});

submitButton.addEventListener("mouseover", function () {

  // Change le style de la souris
  this.style.cursor = "pointer";

  // met en surbrillance la cible de mouseover
  this.style.backgroundColor = "black";
}, false);

submitButton.addEventListener("mouseout", function () {
  // met en surbrillance la cible de mouseover
  this.style.backgroundColor = "green";
}, false);


</script>
