---
title: Randonnée à pas de fourmi
date: 2018-07-19T16:02:02+02:00
lastmod: 2018-07-19T16:02:02+02:00
cover: "/images/BDs/Randonnee_fourmi/cover.jpg"
draft: false
categories: "bandes dessinées"
description: "Bol d'air moelleux en montagne"
---


![planche 1](/images/BDs/Randonnee_fourmi/1-1.jpg)
![planche 1bis](/images/BDs/Randonnee_fourmi/1-2.jpg)
![planche 2](/images/BDs/Randonnee_fourmi/2-1.jpg)
![planche 2bis](/images/BDs/Randonnee_fourmi/2-2.jpg)
![planche 3](/images/BDs/Randonnee_fourmi/3-1.jpg)
![planche 3bis](/images/BDs/Randonnee_fourmi/3-2.jpg)
![Fin](/images/BDs/Fake_News/Fin.jpg)
