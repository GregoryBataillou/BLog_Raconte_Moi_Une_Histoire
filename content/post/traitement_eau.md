---
title: "Le Traitement de l'eau"
date: 2019-10-31T14:14:22+01:00
draft: false
categories: "bandes dessinées"
cover: "/images/BDs/Traitement_eau/0-couverture-1.png"
tags:
- Professeur Bactériophage
- bandes dessinées
description: "Comment fonctionne une station d'épuration"
---

Nous utilisons tous les jours de l'eau potable en grande quantité. C'est un luxe! mais que fait-on de l'eau qu'on rejette?
Le professeur Henri Bactériophage et son assistant Georges nous en dit un peu plus sur les stations d'épurations et leurs fonctionnements!

Pour ceux qui souhaiteraient l'intégralité de la BD en version pdf : [lien](/images/BDs/Traitement_eau/Livre_Traitement_eau.pdf)


<img src='/images/BDs/Traitement_eau/1.png' alt='Vignette 1' width='1000'>
<img src='/images/BDs/Traitement_eau/2.png' alt='Vignette 2' width='1000'>
<img src='/images/BDs/Traitement_eau/3.png' alt='Vignette 3' width='1000'>
<img src='/images/BDs/Traitement_eau/4.png' alt='Vignette 4' width='1000'>
<img src='/images/BDs/Traitement_eau/5.png' alt='Vignette 5' width='1000'>
<img src='/images/BDs/Traitement_eau/6.png' alt='Vignette 6' width='1000'>
<img src='/images/BDs/Traitement_eau/7.png' alt='Vignette 7' width='1000'>
<img src='/images/BDs/Traitement_eau/8.png' alt='Vignette 8' width='1000'>
<img src='/images/BDs/Traitement_eau/9.png' alt='Vignette 9' width='1000'>
<img src='/images/BDs/Traitement_eau/10.png' alt='Vignette 10' width='1000'>
<img src='/images/BDs/Traitement_eau/11.png' alt='Vignette 11' width='1000'>
<img src='/images/BDs/Traitement_eau/12.png' alt='Vignette 12' width='1000'>
<img src='/images/BDs/Traitement_eau/13.png' alt='Vignette 13' width='1000'>
<img src='/images/BDs/Traitement_eau/14.png' alt='Vignette 14' width='1000'>
<img src='/images/BDs/Traitement_eau/15.png' alt='Vignette 15' width='1000'>
<img src='/images/BDs/Traitement_eau/16.png' alt='Vignette 16' width='1000'>
<img src='/images/BDs/Traitement_eau/17.png' alt='Vignette 17' width='1000'>
<img src='/images/BDs/Traitement_eau/18.png' alt='Vignette 18' width='1000'>
<img src='/images/BDs/Traitement_eau/19.png' alt='Vignette 19' width='1000'>
<img src='/images/BDs/Traitement_eau/20.png' alt='Vignette 20' width='1000'>
<img src='/images/BDs/Traitement_eau/21.png' alt='Vignette 21' width='1000'>
<img src='/images/BDs/Traitement_eau/22.png' alt='Vignette 22' width='1000'>
<img src='/images/BDs/Traitement_eau/23.png' alt='Vignette 23' width='1000'>
<img src='/images/BDs/Traitement_eau/24.png' alt='Vignette 24' width='1000'>
<img src='/images/BDs/Traitement_eau/25.png' alt='Vignette 25' width='1000'>
<img src='/images/BDs/Traitement_eau/26.png' alt='Vignette 26' width='1000'>
<img src='/images/BDs/Traitement_eau/27.png' alt='Vignette 27' width='1000'>
<img src='/images/BDs/Traitement_eau/28.png' alt='Vignette 28' width='1000'>
<img src='/images/BDs/Traitement_eau/29.png' alt='Vignette 29' width='1000'>
<img src='/images/BDs/Traitement_eau/30.png' alt='Vignette 30' width='1000'>
<img src='/images/BDs/Traitement_eau/31.png' alt='Vignette 31' width='1000'>
<img src='/images/BDs/Traitement_eau/32.png' alt='Vignette 32' width='1000'>
<img src='/images/BDs/Traitement_eau/33.png' alt='Vignette 33' width='1000'>
<img src='/images/BDs/Traitement_eau/34.png' alt='Vignette 34' width='1000'>
<img src='/images/BDs/Traitement_eau/35.png' alt='Vignette 35' width='1000'>
<img src='/images/BDs/Traitement_eau/36.png' alt='Vignette 36' width='1000'>
<img src='/images/BDs/Traitement_eau/37.png' alt='Vignette 37' width='1000'>
<img src='/images/BDs/Traitement_eau/38.png' alt='Vignette 38' width='1000'>
<img src='/images/BDs/Traitement_eau/39.png' alt='Vignette 39' width='1000'>
<img src='/images/BDs/Traitement_eau/40.png' alt='Vignette 40' width='1000'>
<img src='/images/BDs/Traitement_eau/41.png' alt='Vignette 41' width='1000'>
<img src='/images/BDs/Traitement_eau/42.png' alt='Vignette 42' width='1000'>
<img src='/images/BDs/Traitement_eau/43.png' alt='Vignette 43' width='1000'>
<img src='/images/BDs/Traitement_eau/44.png' alt='Vignette 44' width='1000'>
<img src='/images/BDs/Traitement_eau/45.png' alt='Vignette 45' width='1000'>
<img src='/images/BDs/Traitement_eau/46.png' alt='Vignette 46' width='1000'>
<img src='/images/BDs/Traitement_eau/47.png' alt='Vignette 47' width='1000'>
<img src='/images/BDs/Traitement_eau/48.png' alt='Vignette 48' width='1000'>
<img src='/images/BDs/Traitement_eau/49.png' alt='Vignette 49' width='1000'>
<img src='/images/BDs/Traitement_eau/50.png' alt='Vignette 50' width='1000'>
<img src='/images/BDs/Traitement_eau/51.png' alt='Vignette 51' width='1000'>
<img src='/images/BDs/Traitement_eau/52.png' alt='Vignette 52' width='1000'>
<img src='/images/BDs/Traitement_eau/53.png' alt='Vignette 53' width='1000'>
<img src='/images/BDs/Traitement_eau/54.png' alt='Vignette 54' width='1000'>
<img src='/images/BDs/Traitement_eau/55.png' alt='Vignette 55' width='1000'>
<img src='/images/BDs/Traitement_eau/56.png' alt='Vignette 56' width='1000'>
<img src='/images/BDs/Traitement_eau/57.png' alt='Vignette 57' width='1000'>

<img src='/images/BDs/Traitement_eau/Fin2.png' alt='Vignette de fin' width='1000'>
