---
title: La Communication Non-Violente - épisode 2
date: 2018-10-22T16:31:16+02:00
lastmod: 2018-10-22T16:31:16+02:00
cover: "/images/BDs/CNV/Ep_2/cover.jpg"
draft: false
categories: "bandes dessinées"
tags: "CNV"
description: ou Comment discerner les observations des interprétations
---

Aujoud'hui, ce deuxième épisode de la Communication non-Violente nous fait découvrir la différence
entre un jugement et une observation. Difficile de s'en rendre compte, mais nos paroles mélangent
observations et jugements en permanence. Savoir reconnaître la différence entre les deux est essentielle
pour pouvoir s'intéresser à ses propres besoins et à ceux des autres.

Voilà une illustration pour nous apprendre à bien discerner l'observation de son interprétation.

Bonne lecture!

![Vignette de titre](/images/BDs/CNV/Ep_2/0 - titre.jpg)
![Vignette 1](/images/BDs/CNV/Ep_2/1.jpg)
![Vignette 2](/images/BDs/CNV/Ep_2/2.jpg)
![Vignette 3](/images/BDs/CNV/Ep_2/3.jpg)
![Vignette 4](/images/BDs/CNV/Ep_2/4.jpg)
![Vignette 5](/images/BDs/CNV/Ep_2/5.jpg)
![Vignette 6](/images/BDs/CNV/Ep_2/6.jpg)
![Vignette 7](/images/BDs/CNV/Ep_2/7.jpg)
![Vignette 8](/images/BDs/CNV/Ep_2/8.jpg)
![Vignette 9](/images/BDs/CNV/Ep_2/9.jpg)
![Vignette 10](/images/BDs/CNV/Ep_2/10.jpg)
![Vignette 11](/images/BDs/CNV/Ep_2/11.jpg)
![Vignette 12](/images/BDs/CNV/Ep_2/12.jpg)
![Vignette 13](/images/BDs/CNV/Ep_2/13.jpg)
![Vignette 14](/images/BDs/CNV/Ep_2/14.jpg)

Voici une série de 10 phrases. Certaines sont des observations simples, d'autres sont des jugements (ou un peu des 2).

Cochez les phrases qui vous semblent être des simples observations :

<div id="quizz">
</div>


<input type="submit" value="voir les résultats" id="soumission" style="
    background-color: green;
    border: none;
    color: white;
    padding: 2% 3%;
    margin: 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;">

<script>

function displayTrueOrFalse(tableauReponses, firstTime=false) {

  for (i = 1; i < Object.keys(tableauQuestionsReponses).length + 1; i++) {
    var checkBox = document.getElementById("checkbox"+i);
    var text = document.getElementById("reponse"+i);

    // If the checkbox is checked, display the output text
    if (checkBox.checked == true){
      text.style.display = "block";
      text.style.color = tableauReponses[i][2];
    } else if ((tableauReponses[i][2] === "green" ) && (!firstTime)) {
      text.style.display = "block";
      text.style.color = "red";
    } else {
      text.style.display = "none";
    }
  }
}


var tableauQuestionsReponses = {
  1 : [
    "Hier, Nathanaël était en colère contre moi sans aucune raison",
    "Pas d'accord. Sans aucune raison est un jugement, une interprétation du comportement de l'autre",
    "red"
  ],
  2 : [
    "Hier soir, Florentin s'est rongé les ongles pendant le repas",
    "C'est une simple observation",
    "green"
  ],
  3: [
    "Terence ne m'a pas demandé mon avis pendant la réunion",
    "Simple observation, aucun jugement ici.",
    "green"
  ],
  4 : [
    "Mon père est un homme généreux",
    "Pas d'accord! 'généreux' est un terme sujet à jugement",
    "red"
  ],
  5 : [
    "Eléonore travaille trop",
    "'Trop' est clairement un jugement. On aurait pu dire 'Je n\'ai pas vu Eléonore rentrer avant 21h du boulot cette semaine.",
    "red"
  ],
  6 : [
    "Pierre est agressif",
    "'Agressif' est un mot qui veut dire tout et n'importe quoi. Est-ce qu'il frappe quelqu'un? Est-ce qu'il parle plus fort que toi?",
    "red"
  ],
  7: [
    "Eva est arrivée la première tous les jours de la semaine",
    "C'est une observation",
    "green"
  ],
  8 : [
    "Il arrive souvent que mon frère ne se brosse pas les dents.",
    "'Souvent' est un jugement. Une observation pourrait être : 'Je l'ai vu se brosser les dents qu'une fois cette semaine",
    "red"
  ],
  9 : [
    "Arnaud m'a dit que le violet ne m'allait pas",
    "c'est une observation, on répète ce qu'il dit mot pour mot",
    "green"
  ],
  10 : [
    "Jacques se plaint chaque fois que je parle avec lui",
    "Pas d'accord! Que veut dire 'se plaindre'? c'est un jugement",
    "red"
  ]
}

var submitButton = document.getElementById("soumission");
var quizzCadre = document.getElementById("quizz");


for (i=1; i<Object.keys(tableauQuestionsReponses).length + 1; i ++) {

  var inputI = document.createElement("INPUT");
  inputI.setAttribute("type", "checkbox");
  inputI.setAttribute("id", "checkbox" + i);
  inputI.setAttribute("name", "checkbox" + i);  

  var labelI = document.createElement("LABEL");
  labelI.setAttribute("for", "checkbox" + i);
  var observationI = document.createTextNode("\n" + tableauQuestionsReponses[i][0] + "\n")
  labelI.appendChild(observationI);

  var reponseI = document.createElement("P");
  reponseI.setAttribute("id", "reponse" + i);
  var reponseTextI = document.createTextNode("\n" + tableauQuestionsReponses[i][1] + "\n")
  reponseI.appendChild(reponseTextI);

  var divInputI = document.createElement("DIV");
  divInputI.appendChild(inputI);
  divInputI.appendChild(labelI);
  divInputI.appendChild(reponseI);

  quizzCadre.appendChild(divInputI);

};

displayTrueOrFalse(tableauQuestionsReponses, firstTime=true)

submitButton.addEventListener("click", function () {
  if (this.value === "voir les résultats") {
    displayTrueOrFalse(tableauQuestionsReponses);
  }
});

submitButton.addEventListener("mouseover", function () {

  // Change le style de la souris
  this.style.cursor = "pointer";

  // met en surbrillance la cible de mouseover
  this.style.backgroundColor = "black";
}, false);

submitButton.addEventListener("mouseout", function () {
  // met en surbrillance la cible de mouseover
  this.style.backgroundColor = "green";
}, false);


</script>
