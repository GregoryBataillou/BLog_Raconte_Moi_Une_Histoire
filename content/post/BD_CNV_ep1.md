---
title: La Communication Non-Violente - épisode 1
date: 2018-10-22T15:57:29+02:00
lastmod: 2018-10-22T15:57:29+02:00
cover: "/images/BDs/CNV/Ep_1/cover.jpg"
draft: false
categories: "bandes dessinées"
tags: "CNV"
description: ou Comment résoudre les conflits par un nouveau mode de communication
---

Aujourd'hui, on aborde un nouveau thème passionnant : la Communication Non-Violente!

Ce mode de communication résulte du travail du psychologue américain [Marshall Rosenberg](https://fr.wikipedia.org/wiki/Marshall_Rosenberg). Directeur et fondateur de
l'ONG "Center for Non-Violent Communication", il est l'auteur de nombreux livres à
ce sujet, notamment "Les mots sont des fenêtres (ou bien ce sont des murs)".
C'est sur ce livre, qui décrit en détail le processus de communication non-violente,
que je m'appuie pour réaliser ce billet.

Dans ce premier épisode, nous abordons la notion de langage aliénant. C'est un mode
de communication qu'on utilise pour exprimer un sentiment ou un besoin, mais qui
ne se comprend malheureusement pas facilement. Une image vaut mieux que mille mots,
donc je vous laisse découvrir ce langage en illustrations.

Bonne lecture!

![Vignette de titre](/images/BDs/CNV/Ep_1/0 - titre.jpg)
![Vignette 1](/images/BDs/CNV/Ep_1/1.jpg)
![Vignette 2](/images/BDs/CNV/Ep_1/2.jpg)
![Vignette 3](/images/BDs/CNV/Ep_1/3.jpg)
![Vignette 4](/images/BDs/CNV/Ep_1/4.jpg)
![Vignette 5](/images/BDs/CNV/Ep_1/5.jpg)
![Vignette 6](/images/BDs/CNV/Ep_1/6.jpg)
![Vignette 7](/images/BDs/CNV/Ep_1/7.jpg)
![Vignette 8](/images/BDs/CNV/Ep_1/8.jpg)
![Vignette 9](/images/BDs/CNV/Ep_1/9.jpg)
![Vignette 10](/images/BDs/CNV/Ep_1/10.jpg)
![Vignette 11](/images/BDs/CNV/Ep_1/11.jpg)
![Vignette 12](/images/BDs/CNV/Ep_1/12.jpg)
![Vignette 13](/images/BDs/CNV/Ep_1/13.jpg)
![Vignette 14](/images/BDs/CNV/Ep_1/14.jpg)
![Vignette 15](/images/BDs/CNV/Ep_1/15.jpg)
![Vignette 16](/images/BDs/CNV/Ep_1/16.jpg)
![Vignette 17](/images/BDs/CNV/Ep_1/17.jpg)
