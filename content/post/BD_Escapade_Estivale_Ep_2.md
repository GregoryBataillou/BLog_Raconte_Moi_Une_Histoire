---
title: "BD : Escapade Estivale - Episode 2"
date: 2018-09-09T21:05:41+02:00
lastmod: 2018-09-09T21:05:41+02:00
cover: "/images/BDs/Escapade_estivale/Episode_2/cover.jpg"
draft: false
categories: "bandes dessinées"
tags: "Escapade Estivale"
description: "La suite de l'aventure tout aussi inédite!"
---
Voilà la suite de mon aventure commencée tout nu dans un champs de figuier ! Si vous n'avez pas lu le premier épisode, je vous conseille de faire un tour [ici](/post/bd_escapade_estivale_ep_1/) avant de continuer!

**Objectif de cet épisode** : Trouver un hébergement pour le soir chez l'habitant.

Si vous préférez le format BD, Les planches sont téléchargeables ci dessous :

- [planche 1](/images/BDs/Escapade_estivale/Episode_2/planche_1.jpg)
- [planche 2](/images/BDs/Escapade_estivale/Episode_2/planche_2.jpg)
- [planche 3](/images/BDs/Escapade_estivale/Episode_2/planche_3.jpg)
- [planche 4](/images/BDs/Escapade_estivale/Episode_2/planche_4.jpg)

Bonne lecture!!!
![planche 1](/images/BDs/Escapade_estivale/Episode_2/1.jpg)
![planche 2](/images/BDs/Escapade_estivale/Episode_2/2.jpg)
![planche 3](/images/BDs/Escapade_estivale/Episode_2/3.jpg)
![planche 4](/images/BDs/Escapade_estivale/Episode_2/4.jpg)
![planche 5](/images/BDs/Escapade_estivale/Episode_2/5.jpg)
![planche 6](/images/BDs/Escapade_estivale/Episode_2/6.jpg)
![planche 7](/images/BDs/Escapade_estivale/Episode_2/7.jpg)
![planche 8](/images/BDs/Escapade_estivale/Episode_2/8.jpg)
![planche 9](/images/BDs/Escapade_estivale/Episode_2/9.jpg)
![planche 10](/images/BDs/Escapade_estivale/Episode_2/10.jpg)
![planche 11](/images/BDs/Escapade_estivale/Episode_2/11.jpg)
![planche 12](/images/BDs/Escapade_estivale/Episode_2/12.jpg)
![planche 13](/images/BDs/Escapade_estivale/Episode_2/13.jpg)
![planche 14](/images/BDs/Escapade_estivale/Episode_2/14.jpg)
![planche 15](/images/BDs/Escapade_estivale/Episode_2/15.jpg)
![planche 16](/images/BDs/Escapade_estivale/Episode_2/16.jpg)
![planche 17](/images/BDs/Escapade_estivale/Episode_2/17.jpg)
![planche 18](/images/BDs/Escapade_estivale/Episode_2/18.jpg)
![planche 19](/images/BDs/Escapade_estivale/Episode_2/19.jpg)
![planche 20](/images/BDs/Escapade_estivale/Episode_2/20.jpg)
![planche 21](/images/BDs/Escapade_estivale/Episode_2/21.jpg)
![planche 22](/images/BDs/Escapade_estivale/Episode_2/22.jpg)
![planche 23](/images/BDs/Escapade_estivale/Episode_2/23.jpg)

Merci à Emilie pour m'avoir ouvert ses portes et avoir eu confiance en moi dès le début, merci à Arnaud pour toute sa connaissance sur l'histoire des alentours, et MERCI à Gilbert et sa femme qui m'ont offert l'hospitalité avec un sens de l'accueil mémorable!

To be continued..
