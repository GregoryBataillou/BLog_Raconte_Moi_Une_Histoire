---
title: "Boule Bleue joue au golf"
tags:
- Boule Bleue
categories:
- Stop Motion
description : "We like it here"
cover : "/images/StopMotion/Boule_Bleue/coverGolf.jpg"
date: 2018-08-10T14:22:50+02:00
draft: false
---

Aujourd'hui, Boule bleue nous fait une démonstration de ses talents de golfeuse.
Amateurs, en profiter.


{{< youtube f5eN6--7iVw >}}

Si vous voulez en savoir plus sur le golf et les hobbies de Boule Bleue, n'oubliez pas de vous abonner à la [chaîne youtube](https://www.youtube.com/channel/UCYz8h8BpTC7wFH77MlbDLSA") !
