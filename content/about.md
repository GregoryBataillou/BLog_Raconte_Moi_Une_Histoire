<h1>Qu'est-ce que hou la la de mais c'est quoi ce blog ?</h1>

----
<br>
Raconte-moi quelque chose est un blog présentant du **contenu artistique** de tout genre autour du principe de raconter une histoire.

<img src="/images/BDs/Interpretation/BD_Youki_1.jpg"
      alt="Image de mise en suspense"
      width="300px"
      title="Exemple de mise en suspense"
      style="margin-left: auto; margin-right: auto; display:block"
/>

Tout le monde raffole de petites histoires, qu'elles soient pédagogiques, morales ou juste drôles ou même loufoques ou encore jolies. 
Tu trouveras ici des images, bandes dessinées, vidéos stop motion de pâte à modeler. Il y en a pour tous les goûts. Cadeau.

<img src="/images/BDs/Interpretation/BD_Youki_2.jpg"
      alt="Image de dénouement"
      width="300px"
      title="Dénouement"
      style="margin-left: auto; margin-right: auto; display:block"
/>

Tu aimes le concept? N'hésites pas à partager, aimer mes histoires, t'abonner, partager sur les liens en haut!
